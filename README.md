# parse-autogen

Parse AutoGen .def file and output as JSON and Texinfo

## Usage

```console
$ cargo run fixtures/gnutls/certtool-args.def
```

## TODO

- JSON schema for the generated files
- Preprocessor macros; maybe we don't need them

## License

MIT
